package com.example.daniel.viewspart2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;

public class ProgresDialog extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progres_dialog);

        final ProgressDialog dialog = ProgressDialog.show(this,"Progress Dialog","Pensando um Pouco",false,true);

        new Thread() {
            public void run() {
                try {
                    Thread.sleep(9000);
                    dialog.dismiss();
                }catch (Exception e) {
                    e.getMessage();
                }
            }
        }.start();
    }

    public void back(View view) {
        Intent intent = new Intent(view.getContext(),MainActivity.class);
        startActivity(intent);
    }
}
