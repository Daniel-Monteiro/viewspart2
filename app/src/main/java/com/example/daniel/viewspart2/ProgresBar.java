package com.example.daniel.viewspart2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgresBar extends Activity{

    protected static final int FINAL_TIME = 10000;
    protected int progresso = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progres_bar);

        final ProgressBar progresBar = (ProgressBar) findViewById(R.id.progressBar);

         new Thread() {
            @Override
            public void run() {
                try {
                    while (progresso <= 100) {
                        Thread.sleep(700);
                        progresso +=5;
                        progresBar.setProgress(progresso);
                    }
                }catch(Exception e){
                    e.getMessage();
                }
            }
        }.start();
    }

    public void back2(View view){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
