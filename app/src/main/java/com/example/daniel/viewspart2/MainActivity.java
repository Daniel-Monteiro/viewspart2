package com.example.daniel.viewspart2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void progressDialog(View view) {
        Intent intent = new Intent(this, ProgresDialog.class);
        startActivity(intent);
    }

    public void progresBar(View view) {
        Intent intent = new Intent(this, ProgresBar.class);
        startActivity(intent);
    }

    public void listaItem(View view){
        Intent intent = new Intent(this,ListaView.class);
        startActivity(intent);
    }

    public void btnAlerts(View view){
        Intent intent = new Intent(this,Alerts.class);
        startActivity(intent);
    }
}
