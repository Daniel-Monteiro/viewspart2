package com.example.daniel.viewspart2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListaView extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        List<String> modelos = new ArrayList();
        modelos.add("Les Paul");
        modelos.add("Stratocaster");
        modelos.add("Telecaster");
        modelos.add("Super Strato");
        modelos.add("Flying V");

        ListView listView = (ListView) findViewById(R.id.listaItensDinamica);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,modelos);

        listView.setAdapter(adapter);
    }

    public void btnlistaItem(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
