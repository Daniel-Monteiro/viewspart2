package com.example.daniel.viewspart2;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListViewCustom extends Activity{

    int [] IMAGES = {R.drawable.flying_v, R.drawable.lespaul,R.drawable.stratocaster,R.drawable.superstrato,R.drawable.telecaster};
    String[] GUITARS = {"Flying V", "Les Paul","Stratocaster", "Superstrato", "Telecaster"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        ListView list = (ListView) findViewById(R.id.listaItensDinamica);

        list.setAdapter(new CustomAdapter());
    }

    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            view = getLayoutInflater().inflate(R.layout.custom_list,null);

            ImageView img = (ImageView) findViewById(R.id.imageView_Guitar);
            TextView txt = (TextView) findViewById(R.id.textView_Guitar);


//            txt.setText(GUITARS[position]);
//            img.setImageResource(IMAGES[position]);

            return view;
        }
    }
}
