package com.example.daniel.viewspart2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

public class Alerts extends Activity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alerts);

        new AlertDialog.Builder(this).setTitle("Alertas")
                .setMessage("Consegue Visualizar?")
                .setPositiveButton("sim",null)
                .setNegativeButton("Não",null)
                .setNeutralButton("Não Tenho Certeza",null)
                .show();
    }


    public void back3(View view) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

}
